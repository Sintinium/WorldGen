package com.gmail.sintinium.worldgenerator.biome

import org.bukkit.block.Biome

enum class Biomes(val baseBiome: BaseBiome) {
//    EXTREME_HILLS(ExtremeHills()),

    PLAINS(BaseBiome(Biome.PLAINS, .5f, .35f, 60f, 76f)),
    CANYONS(CanyonsBiome())
}