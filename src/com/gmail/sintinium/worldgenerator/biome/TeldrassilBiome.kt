package com.gmail.sintinium.worldgenerator.biome

import org.bukkit.block.Biome

class TeldrassilBiome : BaseBiome(Biome.FOREST, .75f, .65f) {

    init {
        setNoiseValues(.2, 2.0, 3.0)
    }

}