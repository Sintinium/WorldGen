package com.gmail.sintinium.worldgenerator.biome

import com.gmail.sintinium.worldgenerator.chunk.chunkGenerator
import org.bukkit.Material
import org.bukkit.block.Biome
import java.util.*

class CanyonsBiome : BaseBiome(Biome.DESERT, .02f, .85f) {

    companion object {
        val random = Random()
    }

    init {
        setNoiseValues(.7, 1.0, 0.0)
        baseHeight = 80
        smoothRadius = 10
    }

    override fun getLayerBlock(x: Double, y: Double, z: Double, columnHeight: Double): MaterialResult {
        return Material.RED_SANDSTONE.toMaterialResult()
    }

    override fun getHeight(x: Double, z: Double): Double {
        val canyonSpikeHeight = Math.abs(chunkGenerator.terrain.height2.noise(x, z, frequency, amplitude) * 1.2)
        val detail = chunkGenerator.terrain.height2.noise(x, z, frequency, 1.3) * 5.0
        val multi = (5 * Math.pow(canyonSpikeHeight, 2.0) + 4)
        val layerCount = Math.abs(chunkGenerator.terrain.height2.noise(x, z, .3, amplitude) * 1.5) * 5.0
        val canyon = (Math.abs(Math.round(chunkGenerator.terrain.height2.noise(x, z, frequency, amplitude) * layerCount) * 2) / 2.0) * multi + baseHeight + detail
        return canyon
    }
}