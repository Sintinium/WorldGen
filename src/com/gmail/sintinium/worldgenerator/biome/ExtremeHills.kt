package com.gmail.sintinium.worldgenerator.biome

import com.gmail.sintinium.worldgenerator.chunk.chunkGenerator
import org.bukkit.Material
import org.bukkit.block.Biome

class ExtremeHills : BaseBiome(Biome.EXTREME_HILLS, .50f, .65f) {

    init {
        setNoiseValues(1.2, 1.5, 6.0)
        topLayer = arrayOf(Material.STONE)
        smoothRadius = 10
    }

    override fun getHeight(x: Double, z: Double): Double {
        val h1 = (chunkGenerator.terrain.height1.noise(x, z, frequency, amplitude) + amplitude / 2.0) * multitude + baseHeight
        //TODO: Make flatHeight not go down so much (currently goes down below y level 40
        val flatHeight = (chunkGenerator.terrain.height1.noise(x, z, frequency, 1.5) + 1.5 / 2.0) * 3 + baseHeight
        return Math.max(h1, flatHeight)
    }
}