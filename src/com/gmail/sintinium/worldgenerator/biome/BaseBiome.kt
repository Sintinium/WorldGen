package com.gmail.sintinium.worldgenerator.biome

import com.gmail.sintinium.worldgenerator.chunk.chunkGenerator
import org.bukkit.Material
import org.bukkit.Material.*
import org.bukkit.block.Biome

open class BaseBiome(val biome: Biome, val rainfall: Float, val temperature: Float, val minElevation: Float = 0.0f, val maxElevation: Float = 999f) {

    var frequency = .5
    var amplitude = .5
    var multitude = 3.0
    var baseHeight = 64

    /**
     * Used in default [getLayerBlock] method. Can safely be ignored if overriding method.
     */
    var topLayer = arrayOf(GRASS, DIRT, DIRT, DIRT, DIRT)

    /**
     * Radius the generator should smooth this biome with nearby biomes
     */
    var smoothRadius = 5

    protected fun setNoiseValues(frequency: Double, amplitude: Double, multitude: Double) {
        this.frequency = frequency
        this.amplitude = amplitude
        this.multitude = multitude
    }

    /**
     * Returns how close the biome matches the rainfall and temperature squared.
     * NOTE: This result should have [Math.sqrt] applied to it before using it for anything other than comparing.
     */
    open fun getSquaredCloseness(rainfall: Double, temperature: Double): Double {
        return Math.pow(this.rainfall.toDouble() - rainfall, 2.0) + Math.pow(this.temperature.toDouble() - temperature, 2.0)
    }

    /**
     * Returns if the current elevation is suitable for this biome
     */
    open fun isValidElevation(elevation: Double): Boolean = (elevation in minElevation..maxElevation)

    open fun canSnow(elevation: Double) = temperature < .15 || (temperature <= 0.3 && elevation >= 130)

    open fun canRain(): Boolean = (temperature in 0.15..0.95)

    open fun getLayerBlock(x: Double, y: Double, z: Double, columnHeight: Double): MaterialResult {
        val distanceUntilTop = getDistanceUntilTop(y, columnHeight)

        return if (distanceUntilTop < topLayer.size) {
            topLayer[distanceUntilTop].toMaterialResult()
        } else {
            STONE.toMaterialResult()
        }
    }

    open fun getHeight(x: Double, z: Double): Double {
        return (chunkGenerator.terrain.height1.noise(x, z, frequency, amplitude) + amplitude / 2.0) * multitude + baseHeight
    }

    protected fun getDistanceUntilTop(y: Double, columnHeight: Double) = columnHeight.toInt() - y.toInt()

    data class MaterialResult(val material: Material, val materialData: Byte? = null)

    fun Material.toMaterialResult(materialData: Byte? = null): MaterialResult = MaterialResult(this, materialData)
}

