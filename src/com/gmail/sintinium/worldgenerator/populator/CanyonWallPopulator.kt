package com.gmail.sintinium.worldgenerator.populator

import com.gmail.sintinium.worldgenerator.GenerationTerrain
import com.gmail.sintinium.worldgenerator.biome.Biomes
import com.gmail.sintinium.worldgenerator.worldGenerator
import org.bukkit.Chunk
import org.bukkit.Material
import org.bukkit.World
import org.bukkit.block.Block
import org.bukkit.generator.BlockPopulator
import org.bukkit.material.MaterialData
import org.bukkit.material.Wool
import java.util.*
import kotlin.system.measureTimeMillis

class CanyonWallPopulator : BlockPopulator() {

    override fun populate(world: World, random: Random, chunk: Chunk) {
        val terrain = GenerationTerrain(world.seed)
        val chunkX = chunk.x
        val chunkZ = chunk.z
        for (x in 0 until 16) {
            for (z in 0 until 16) {
                val realX = chunkX * 16 + x
                val realZ = chunkZ * 16 + z

                //TODO: Add caching of current biome to save performance for future populators!!!
                //Check if biome is canyons
                var topBlock = world.getHighestBlockAt(realX, realZ)
                val biome = terrain.getClosestBiome(topBlock.x, topBlock.z, elevation = topBlock.y.toDouble())
                if (biome != Biomes.CANYONS) continue

                //Gets the top block. If top block is a slab we go down until we hit a solid block
                val underBlock = world.getBlockAt(topBlock.x, topBlock.y - 1, topBlock.z)
                if (underBlock.type == Material.STONE_SLAB2) {
                    topBlock = underBlock
                    while (topBlock.type == Material.STONE_SLAB2 || topBlock.type == Material.AIR) {
                        topBlock = world.getBlockAt(topBlock.x, topBlock.y - 1, topBlock.z)
                    }
                    topBlock = world.getBlockAt(topBlock.x, topBlock.y + 1, topBlock.z)
                }

                //TODO: Once caching is added check if current biome is still canyons, if so call [fillAirBelow] to prevent hanging slabs
                //This is where we place the walls and slabs
                val hasSide = checkSides(world, topBlock.x, topBlock.y, topBlock.z)
                var index = 0
                if (hasSide) {
                    while (true) {
                        val currentBlock = world.getBlockAt(topBlock.x, topBlock.y + index, topBlock.z)
                        if (!checkSides(world, currentBlock.x, currentBlock.y, currentBlock.z)) {
                            break
                        }

                        if (index == 0) {
                            populateSideSlabs(world, currentBlock)
                        }

                        currentBlock.type = Material.STAINED_CLAY
                        currentBlock.data = 14
                        index++
                    }
                }
            }
        }
    }

    private fun Long.time() = System.currentTimeMillis() - this

    private fun checkSides(world: World, x: Int, y: Int, z: Int): Boolean {
        for (dx in -1..1) {
            if (dx == 0) continue
            val block = world.getBlockAt(x + dx, y, z)
            if (block.type == Material.RED_SANDSTONE) return true
        }
        for (dz in -1..1) {
            if (dz == 0) continue
            val block = world.getBlockAt(x, y, z + dz)
            if (block.type == Material.RED_SANDSTONE) return true
        }
        return false
    }

    private fun populateSideSlabs(world: World, baseBlock: Block) {
        for (dx in -1..1) {
            if (dx == 0) continue
            val currentBlock = world.getBlockAt(baseBlock.x + dx, baseBlock.y, baseBlock.z)

            if (currentBlock.type == Material.AIR) {
                currentBlock.type = Material.STONE_SLAB2
            }
//            fillAirBelow(world, currentBlock)
        }
        for (dz in -1..1) {
            if (dz == 0) continue
            val currentBlock = world.getBlockAt(baseBlock.x, baseBlock.y, baseBlock.z + dz)
            if (currentBlock.type == Material.AIR) {
                currentBlock.type = Material.STONE_SLAB2
            }
//            fillAirBelow(world, currentBlock)
        }
    }

    /**
     * Replaces all air blocks under block to air. Doesn't change the [start] block
     */
    private fun fillAirBelow(world: World, start: Block) {
        var block = world.getBlockAt(start.x, start.y - 1, start.z)
        if (block.type != Material.AIR) return
        while (block.type == Material.AIR) {
            block.type = Material.STAINED_CLAY
            block.data = 14
            block = world.getBlockAt(block.x, block.y - 1, block.z)
        }
        populateSideSlabs(world, block)
    }

}