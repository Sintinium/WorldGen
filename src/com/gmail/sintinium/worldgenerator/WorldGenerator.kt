package com.gmail.sintinium.worldgenerator

import com.gmail.sintinium.kspigot.util.LOGGER
import com.gmail.sintinium.kspigot.util.SERVER
import com.gmail.sintinium.kspigot.util.runTaskLater
import com.gmail.sintinium.worldgenerator.biome.Biomes
import com.gmail.sintinium.worldgenerator.chunk.CustomChunkGenerator
import org.bukkit.World
import org.bukkit.WorldCreator
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld
import org.bukkit.entity.Player
import org.bukkit.generator.ChunkGenerator
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import java.util.*
import java.util.logging.Level
import kotlin.properties.Delegates
import kotlin.reflect.jvm.javaField

var worldGenerator by Delegates.notNull<WorldGenerator>()

class WorldGenerator : JavaPlugin() {

    /**
     * Gets the closest biome based on rainfall, temperature, and (optionally) elevation
     * @param elevation used to prevent biomes going too high or tow low. (Set to null if ignore elevation)
     */
    fun getClosestBiome(rainfall: Double, temperature: Double, elevation: Double? = null): Biomes {
        var closestBiome: Biomes? = null
        var lastDist = Double.MAX_VALUE
        for (b in Biomes.values()) {
            if (elevation != null && !b.baseBiome.isValidElevation(elevation)) continue
            val closeness = b.baseBiome.getSquaredCloseness(rainfall, temperature)
            if (closeness <= lastDist || closestBiome == null) {
                closestBiome = b
                lastDist = closeness
            }
        }
        return closestBiome!!
    }

    override fun onEnable() {
        worldGenerator = this
    }

    override fun getDefaultWorldGenerator(worldName: String?, id: String?): ChunkGenerator = CustomChunkGenerator()

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<String>): Boolean {
        if (sender !is Player) {
            sender.sendMessage("Player only command")
            return true
        }
        if (!command.name.equals("genworld", true)) {
            return false
        }
        val clazz = sender.world.javaClass
        val field = clazz.getDeclaredField("generator")
        field.isAccessible = true
        field.set(sender.world, CustomChunkGenerator())

//        var worldName = "world_test"
//        var seed = 2L
//        when (args.size) {
//            1 -> worldName = args[0]
//            2 -> {
//                worldName = args[0]
//                seed = args[1].toLong()
//            }
//            else -> {
//            }
//        }
//        sender.sendMessage("Generating world...")
//        val oldWorld = SERVER.worlds.filter { it.name.equals(worldName, true) }.firstOrNull()
//        if (oldWorld != null) {
//            sender.sendMessage("Deleting old world...")
//            unloadAndDelete(oldWorld)
//        }
//        val world = WorldCreator.name(worldName).environment(World.Environment.NORMAL).seed(seed).generator(CustomChunkGenerator()).createWorld()
//        sender.sendMessage("Finished, teleporting to spawn of world")
//        sender.teleport(world.spawnLocation)
        return true
    }

    fun unloadAndDelete(world: World) {
        val path = File(world.worldFolder.parent, world.name)
        world.players.forEach {
            it.teleport(SERVER.worlds.first().spawnLocation)
        }
        SERVER.unloadWorld(world, false)
        LOGGER.log(Level.INFO, "Deleting world at $path")
        path.deleteRecursively()
    }

}