package com.gmail.sintinium.worldgenerator.noise

import java.util.Random

/*
 * Copyright (C) 2003, 2004 Jason Bevins (original libnoise code)
 * Copyright © 2010 Thomas J. Hodge (java port of libnoise)
 *
 * This file was part of libnoiseforjava.
 *
 * libnoiseforjava is a Java port of the C++ library libnoise, which may be found at
 * http://libnoise.sourceforge.net/.  libnoise was developed by Jason Bevins, who may be
 * contacted at jlbezigvins@gmzigail.com (for great email, take off every 'zig').
 * Porting to Java was done by Thomas Hodge, who may be contacted at
 * libnoisezagforjava@gzagmail.com (remove every 'zag').
 *
 * libnoiseforjava is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * libnoiseforjava is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * libnoiseforjava.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
class VoronoiNoiseGenerator(var seed: Long, var distanceMethod: Short) {

    //You can either use the feature point height (for biomes or solid pillars), or the distance to the feature point
    var isUseDistance = false

    private fun getDistance(xDist: Double, zDist: Double): Double {
        return when (distanceMethod) {
            0.toShort() -> Math.sqrt(xDist * xDist + zDist * zDist) / SQRT_2
            1.toShort() -> xDist + zDist
            else -> java.lang.Double.NaN
        }
    }

    private fun getDistance(xDist: Double, yDist: Double, zDist: Double): Double {
        return when (distanceMethod) {
            0.toShort() -> Math.sqrt(xDist * xDist + yDist * yDist + zDist * zDist) / SQRT_3 //Approximation (for speed) of elucidean (regular) distance
            1.toShort() -> xDist + yDist + zDist
            else -> java.lang.Double.NaN
        }
    }

    fun noise(x: Double, z: Double, frequency: Double): Double {
        var x = x
        var z = z
        x *= frequency
        z *= frequency

        val xInt = if (x > .0) x.toInt() else x.toInt() - 1
        val zInt = if (z > .0) z.toInt() else z.toInt() - 1

        var minDist = 32000000.0

        var xCandidate = 0.0
        var zCandidate = 0.0

        for (zCur in zInt - 2..zInt + 2) {
            for (xCur in xInt - 2..xInt + 2) {

                val xPos = xCur + valueNoise2D(xCur, zCur, seed)
                val zPos = zCur + valueNoise2D(xCur, zCur, Random(seed).nextLong())
                val xDist = xPos - x
                val zDist = zPos - z
                val dist = xDist * xDist + zDist * zDist

                if (dist < minDist) {
                    minDist = dist
                    xCandidate = xPos
                    zCandidate = zPos
                }
            }
        }

        if (isUseDistance) {
            val xDist = xCandidate - x
            val zDist = zCandidate - z
            return getDistance(xDist, zDist)
        } else
            return VoronoiNoiseGenerator.valueNoise2D(
                    Math.floor(xCandidate).toInt(),
                    Math.floor(zCandidate).toInt(), seed).toDouble()
    }

    fun noise(x: Double, y: Double, z: Double, frequency: Double): Double {
        var x = x
        var y = y
        var z = z
        // Inside each unit cube, there is a seed point at a random position.  Go
        // through each of the nearby cubes until we find a cube with a seed point
        // that is closest to the specified position.
        x *= frequency
        y *= frequency
        z *= frequency

        val xInt = if (x > .0) x.toInt() else x.toInt() - 1
        val yInt = if (y > .0) y.toInt() else y.toInt() - 1
        val zInt = if (z > .0) z.toInt() else z.toInt() - 1

        var minDist = 32000000.0

        var xCandidate = 0.0
        var yCandidate = 0.0
        var zCandidate = 0.0

        val rand = Random(seed)

        for (zCur in zInt - 2..zInt + 2) {
            for (yCur in yInt - 2..yInt + 2) {
                for (xCur in xInt - 2..xInt + 2) {
                    // Calculate the position and distance to the seed point inside of
                    // this unit cube.

                    val xPos = xCur + valueNoise3D(xCur, yCur, zCur, seed)
                    val yPos = yCur + valueNoise3D(xCur, yCur, zCur, rand.nextLong())
                    val zPos = zCur + valueNoise3D(xCur, yCur, zCur, rand.nextLong())
                    val xDist = xPos - x
                    val yDist = yPos - y
                    val zDist = zPos - z
                    val dist = xDist * xDist + yDist * yDist + zDist * zDist

                    if (dist < minDist) {
                        // This seed point is closer to any others found so far, so record
                        // this seed point.
                        minDist = dist
                        xCandidate = xPos
                        yCandidate = yPos
                        zCandidate = zPos
                    }
                }
            }
        }

        if (isUseDistance) {
            val xDist = xCandidate - x
            val yDist = yCandidate - y
            val zDist = zCandidate - z

            return getDistance(xDist, yDist, zDist)
        } else
            return VoronoiNoiseGenerator.valueNoise3D(
                    Math.floor(xCandidate).toInt(),
                    Math.floor(yCandidate).toInt(),
                    Math.floor(zCandidate).toInt(), seed).toDouble()

    }

    companion object {

        /// Noise module that outputs Voronoi cells.
        ///
        /// In mathematics, a <i>Voronoi cell</i> is a region containing all the
        /// points that are closer to a specific <i>seed point</i> than to any
        /// other seed point.  These cells mesh with one another, producing
        /// polygon-like formations.
        ///
        /// By default, this noise module randomly places a seed point within
        /// each unit cube.  By modifying the <i>frequency</i> of the seed points,
        /// an application can change the distance between seed points.  The
        /// higher the frequency, the closer together this noise module places
        /// the seed points, which reduces the size of the cells.  To specify the
        /// frequency of the cells, call the setFrequency() method.
        ///
        /// This noise module assigns each Voronoi cell with a random constant
        /// value from a coherent-noise function.  The <i>displacement value</i>
        /// controls the range of random values to assign to each cell.  The
        /// range of random values is +/- the displacement value.  Call the
        /// setDisplacement() method to specify the displacement value.
        ///
        /// To modify the random positions of the seed points, call the SetSeed()
        /// method.
        ///
        /// This noise module can optionally add the distance from the nearest
        /// seed to the output value.  To enable this feature, call the
        /// enableDistance() method.  This causes the points in the Voronoi cells
        /// to increase in value the further away that point is from the nearest
        /// seed point.

        //for speed, we can approximate the sqrt term in the distance funtions
        private val SQRT_2 = 1.4142135623730950488
        private val SQRT_3 = 1.7320508075688772935

        /**
         * To avoid having to store the feature points, we use a hash function
         * of the coordinates and the seed instead. Those big scary numbers are
         * arbitrary primes.
         */
        fun valueNoise2D(x: Int, z: Int, seed: Long): Double {
            var n = (1619 * x).toLong() + (6971 * z).toLong() + 1013 * seed and 0x7fffffff
            n = n shr 13 xor n
            return 1.0 - (n * (n * n * 60493 + 19990303) + 1376312589 and 0x7fffffff).toDouble() / 1073741824.0
        }

        fun valueNoise3D(x: Int, y: Int, z: Int, seed: Long): Double {
            var n = (1619 * x).toLong() + (31337 * y).toLong() + (6971 * z).toLong() + 1013 * seed and 0x7fffffff
            n = n shr 13 xor n
            return 1.0 - (n * (n * n * 60493 + 19990303) + 1376312589 and 0x7fffffff).toDouble() / 1073741824.0
        }
    }
}