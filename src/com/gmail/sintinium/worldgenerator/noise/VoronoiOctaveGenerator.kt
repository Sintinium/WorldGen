package com.gmail.sintinium.worldgenerator.noise

import org.bukkit.World

/**
 * A noise generator that uses multiple scaled octaves of Voronoi noise to
 * generate fractal (small features are similar to large ones) noise
 *
 * This is based on the bukkit octave generator, to make it easier to use
 */
class VoronoiOctaveGenerator(seed: Long, numOctaves: Int, useDistance: Boolean = false, var distanceMethod: Short = 0) {

    var xScale: Double = 0.toDouble()
    var yScale: Double = 0.toDouble()
    var zScale: Double = 0.toDouble()
    private val octaves: Array<VoronoiNoiseGenerator?>

    init {
        this.octaves = arrayOfNulls(numOctaves)
        createOctaves(useDistance)
    }

    private fun createOctaves(useDistnace: Boolean) {
        for (i in octaves.indices) {
            octaves[i] = VoronoiNoiseGenerator(0L, distanceMethod)
            octaves[i]!!.isUseDistance = useDistnace
        }
    }

    fun setScale(scale: Double) {
        xScale = scale
        yScale = scale
        zScale = scale
    }

    fun setSeed(seed: Long) {
        for (gen in octaves) {
            gen!!.seed = seed
        }
    }

    fun setWorld(world: World) {
        setSeed(world.seed)
    }

    @JvmOverloads
    fun noise(x: Double, y: Double, frequency: Double, amplitude: Double,
              normalized: Boolean = false): Double {
        var x = x
        var y = y
        var result = 0.0
        var amp = 1.0
        var freq = 1.0
        var max = 0.0
        x *= xScale
        y *= yScale

        for (octave in octaves) {
            result += octave!!.noise(x, y, freq) * amp
            max += amp
            freq *= frequency
            amp *= amplitude
        }
        if (normalized) {
            result /= max
        }

        return result
    }

    /// To generate fractal noise, we calculate the noise of each octave at
    /// an increasingly larger/smaller frequency and amplitude.
    @JvmOverloads
    fun noise(x: Double, y: Double, z: Double, frequency: Double,
              amplitude: Double, normalized: Boolean = false): Double {
        var x = x
        var y = y
        var z = z
        var result = 0.0
        var amp = 1.0
        var freq = 1.0
        var max = 0.0
        x *= xScale
        y *= yScale
        z *= zScale

        for (octave in octaves) {
            result += octave!!.noise(x, y, z, freq) * amp
            max += amp
            freq *= frequency
            amp *= amplitude
        }
        if (normalized) {
            result /= max
        }

        return result
    }
}