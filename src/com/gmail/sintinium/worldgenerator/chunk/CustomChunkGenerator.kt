package com.gmail.sintinium.worldgenerator.chunk

import com.gmail.sintinium.kspigot.util.SERVER
import com.gmail.sintinium.worldgenerator.GenerationTerrain
import com.gmail.sintinium.worldgenerator.biome.Biomes
import com.gmail.sintinium.worldgenerator.populator.CanyonWallPopulator
import com.gmail.sintinium.worldgenerator.worldGenerator
import org.bukkit.Material
import org.bukkit.World
import org.bukkit.generator.BlockPopulator
import org.bukkit.generator.ChunkGenerator
import org.bukkit.material.MaterialData
import java.util.*
import kotlin.properties.Delegates

var chunkGenerator by Delegates.notNull<CustomChunkGenerator>()

class CustomChunkGenerator : ChunkGenerator() {

    lateinit var terrain: GenerationTerrain

    fun initNoise(world: World) {
        chunkGenerator = this
        terrain = GenerationTerrain(world.seed)
    }

    override fun generateChunkData(world: World, random: Random, chunkX: Int, chunkZ: Int, biome: BiomeGrid): ChunkData {
        initNoise(world)
        val chunkData = SERVER.createChunkData(world)

        for (x in 0 until 16) {
            for (z in 0 until 16) {
                val realX = x + chunkX * 16
                val realZ = z + chunkZ * 16
                val xTurb = terrain.turbulence.noise(realX.toDouble(), realZ.toDouble(), .5, 2.5) - .5
                val zTurb = terrain.turbulence.noise(realX.toDouble(), realZ.toDouble(), .7, 1.5) - .5

                val maxHeight = getSmoothHeight(realX + xTurb, realZ + zTurb)

                val closestBiome = terrain.getClosestBiome(realX + xTurb, realZ + xTurb, maxHeight)
                biome.setBiome(x, z, closestBiome.baseBiome.biome)
                for (y in 0..maxHeight.toInt()) {
                    val material = closestBiome.baseBiome.getLayerBlock(realX.toDouble(), y.toDouble(), realZ.toDouble(), maxHeight)
                    if (material.materialData != null) {
                        chunkData.setBlock(x, y, z, MaterialData(material.material, material.materialData))
                    } else {
                        chunkData.setBlock(x, y, z, material.material)
                    }
                }
            }
        }

        chunkData.region(0, 0, 0, 16, 1, 16, Material.BEDROCK)
        return chunkData
    }

    private fun getSmoothHeight(x: Double, z: Double): Double {
        val nearBiomes = mutableMapOf<Biomes, Double>()
        val heights = mutableMapOf<Biomes, Double>()

        val closestBiome = terrain.getClosestBiome(x, z)
        val smoothing = closestBiome.baseBiome.smoothRadius
        putNearBiome(nearBiomes, heights, x, z, smoothing)

        //Getting current biome's smooth radius

        for (dx in 0..smoothing) {
            putNearBiome(nearBiomes, heights, x, z, smoothing, dx = dx)
            putNearBiome(nearBiomes, heights, x, z, smoothing, dx = -dx)
        }
        for (dz in 0..smoothing) {
            putNearBiome(nearBiomes, heights, x, z, smoothing, dz = dz)
            putNearBiome(nearBiomes, heights, x, z, smoothing, dz = -dz)
        }

        if (nearBiomes.size == 1) {
            return heights.values.first()
        }
        var num = 0.0
        var denom = 0.0
        for ((biome, weight) in nearBiomes) {
            num += heights[biome]!! * weight
            denom += weight
        }
        return num / denom
    }

    private fun putNearBiome(nearBiomes: MutableMap<Biomes, Double>, heights: MutableMap<Biomes, Double>, x: Double, z: Double, smoothRadius: Int, dx: Int = 0, dz: Int = 0) {
        val dir = Math.abs(dx + dz.toDouble())
        val biome = terrain.getClosestBiome(x + dx, z + dz)
        val dist = (smoothRadius + 1) - dir
        if (!nearBiomes.containsKey(biome) || nearBiomes[biome]!! < dist) {
            nearBiomes.put(biome, dist)
            heights.put(biome, biome.baseBiome.getHeight(x + dx, z + dz))
        }
    }

    override fun getDefaultPopulators(world: World): MutableList<BlockPopulator> {
        return arrayListOf(CanyonWallPopulator())
    }
}

fun ChunkGenerator.ChunkData.region(xMin: Int, yMin: Int, zMin: Int, xMax: Int, yMax: Int, zMax: Int, material: Material) {
    this.setRegion(xMin, yMin, zMin, xMax, yMax, zMax, material)
}