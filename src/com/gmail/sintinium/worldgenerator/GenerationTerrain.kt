package com.gmail.sintinium.worldgenerator

import com.gmail.sintinium.worldgenerator.biome.Biomes
import com.gmail.sintinium.worldgenerator.noise.VoronoiOctaveGenerator
import org.bukkit.util.noise.SimplexOctaveGenerator

class GenerationTerrain(seed: Long) {

    val temperature = VoronoiOctaveGenerator(seed, 1).apply { setScale(1 / (16.0 * 32.0)) }
    val rainfall = VoronoiOctaveGenerator(seed + 1, 1).apply { setScale(1 / (16.0 * 40.0)) }
    val turbulence = SimplexOctaveGenerator(seed, 5).apply { setScale(1 / 12.0) }

    val height1 = SimplexOctaveGenerator(seed, 1).apply { setScale(1 / 256.0) }
    val height2 = SimplexOctaveGenerator(seed, 1).apply { setScale(1 / 128.0) }

    fun getRainfall(x: Double, z: Double): Double {
        return rainfall.noise(x, z, .75, .5) + .5
    }

    fun getTemperature(x: Double, z: Double): Double {
        return temperature.noise(x, z, .5, .5) + .5
    }

    fun getClosestBiome(x: Int, z: Int, elevation: Double? = null) = getClosestBiome(x.toDouble(), z.toDouble(), elevation)

    fun getClosestBiome(x: Double, z: Double, elevation: Double? = null): Biomes {
        return worldGenerator.getClosestBiome(getRainfall(x, z), getTemperature(x, z), elevation)
    }

}